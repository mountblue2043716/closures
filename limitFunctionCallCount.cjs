function limitFunctionCallCount(cb, n) {
  if (typeof(n) != 'number' || typeof(cb) != 'function') {
    throw new Error ('Invalid Argument');
  }
    let count = 0;
    function check(...args) {
      if (count < n) {
        count++;
        return cb(...args);
      }
      return null;
    };
    return check;
  }
module.exports = limitFunctionCallCount;  