function cacheFunction(cb) {
    if (typeof(cb) !== 'function') {
        throw new Error ('Invalid Argument');
    }
    let cache = {};
    return function(...args) {
        let key = JSON.stringify(args);
        if (key in cache) {
            return cache[key];
        } else {
            let ans = cb(...args);
            cache[key] = ans;
            return ans;
        }
    }
}
module.exports = cacheFunction;
